<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class pageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'slug' => 'home',
            'name' => 'Home',
        ]);

        DB::table('pages')->insert([
            'slug' => 'about_us',
            'name' => 'About Us',
        ]);

        DB::table('pages')->insert([
            'slug' => 'contact_us',
            'name' => 'Contact Us',
        ]);

        DB::table('pages')->insert([
            'slug' => 'our_services',
            'name' => 'Our Services',
        ]);
        DB::table('pages')->insert([
            'slug' => 'our_tour',
            'name' => 'Our Tour',
        ]);

        DB::table('pages')->insert([
            'slug' => 'travel_form',
            'name' => 'Travel Form',
        ]);

        DB::table('pages')->insert([
            'slug' => 'get_quotes',
            'name' => 'Get Quotes',
        ]);

    }
}
