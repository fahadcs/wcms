<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', [App\Http\Controllers\WcmsController::class, 'index'])->name('index');
Route::get('/about-us', [App\Http\Controllers\WcmsController::class, 'about_us'])->name('about_us');
Route::get('/contact-us', [App\Http\Controllers\WcmsController::class, 'contact_us'])->name('contact_us');
Route::get('/our-services', [App\Http\Controllers\WcmsController::class, 'our_services'])->name('our_services');
Route::get('/our-tour', [App\Http\Controllers\WcmsController::class, 'our_tour'])->name('our_tour');
Route::get('/travel-form', [App\Http\Controllers\WcmsController::class, 'travel_form'])->name('travel_form');
Route::get('get_travel_form', [App\Http\Controllers\WcmsController::class, 'get_travel_form'])->name('get_travel_form');
Route::get('/get-quotes', [App\Http\Controllers\WcmsController::class, 'get_quotes'])->name('get_quotes');

Route::post('/contact_us_store', [App\Http\Controllers\WcmsController::class, 'contact_us_store'])->name('contact_us_store');
Route::post('/quotes_store', [App\Http\Controllers\WcmsController::class, 'quotes_store'])->name('quotes_store');
Route::post('/book_tour', [App\Http\Controllers\WcmsController::class, 'book_tour'])->name('book_tour');

Route::get('post/{id}/{slug}', [App\Http\Controllers\BlogController::class, 'single_post'])->name('single_post');
Route::get('posts_by_category', [App\Http\Controllers\BlogController::class, 'posts_by_category'])->name('posts_by_category');
Route::get('all_post/{cate_id?}', [App\Http\Controllers\BlogController::class, 'all_posts'])->name('all_post');
Route::post('comment_store', [App\Http\Controllers\BlogController::class, 'comment_store'])->name('comment.store');



Route::group( [ 'middleware' => 'auth' , 'prefix' => 'admin','as' => 'admin.'], function(){

    Route::get('/dashboard', [App\Http\Controllers\Admin\AdminController::class, 'index'])->name('dashboard');
   
    Route::get('/about_us', [App\Http\Controllers\Admin\AdminController::class, 'about_us'])->name('about_us');
    Route::get('/contact_us', [App\Http\Controllers\Admin\AdminController::class, 'contact_us'])->name('contact_us');
    Route::get('/our_services', [App\Http\Controllers\Admin\AdminController::class, 'our_services'])->name('our_services');
    Route::get('/our_tour', [App\Http\Controllers\Admin\AdminController::class, 'our_tour'])->name('our_tour');
    Route::get('/get_quotes', [App\Http\Controllers\Admin\AdminController::class, 'get_quotes'])->name('get_quotes');
    Route::get('/travel_form', [App\Http\Controllers\Admin\AdminController::class, 'travel_form'])->name('travel_form');
    Route::post('/store_travel_form', [App\Http\Controllers\Admin\AdminController::class, 'store_travel_form'])->name('store_travel_form');

    Route::post('/slider_images_upload', [App\Http\Controllers\Admin\PageController::class, 'slider_images_upload'])->name('slider_images_upload');
    Route::post('/delete_slider_image', [App\Http\Controllers\Admin\PageController::class, 'delete_slider_image'])->name('delete_slider_image');
    Route::post('/home_store', [App\Http\Controllers\Admin\PageController::class, 'home_store'])->name('home_store');
    Route::post('/about_us_store', [App\Http\Controllers\Admin\PageController::class, 'about_us_store'])->name('about_us_store');
    Route::post('/contact_us_store', [App\Http\Controllers\Admin\PageController::class, 'contact_us_store'])->name('contact_us_store');
    Route::post('/our_services_store', [App\Http\Controllers\Admin\PageController::class, 'our_services_store'])->name('our_services_store');
    Route::post('/our_tour_store', [App\Http\Controllers\Admin\PageController::class, 'our_tour_store'])->name('our_tour_store');

    Route::post('/cat_popup', [App\Http\Controllers\CategoryController::class, 'cat_popup'])->name('cat_popup');
    Route::post('/tag_popup', [App\Http\Controllers\TagController::class, 'tag_popup'])->name('tag_popup');
    Route::resource('category', CategoryController::class);
    Route::resource('tag', TagController::class);
    Route::resource('post', PostController::class);
    
    Route::get('ApproveComment/{id}',  [App\Http\Controllers\CommentController::class, 'ApproveComment'])->name('ApproveComment');
    Route::get('DisApproveComment/{id}',  [App\Http\Controllers\CommentController::class, 'DisApproveComment'])->name('DisApproveComment');
    Route::get('DelApproveComment/{id}',  [App\Http\Controllers\CommentController::class, 'DelApproveComment'])->name('DelApproveComment');
    Route::resource('comments', CommentController::class);

});
Auth::routes();


