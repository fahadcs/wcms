-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2021 at 01:30 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_tours`
--

CREATE TABLE `book_tours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cnic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tour_form_json` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_tours`
--

INSERT INTO `book_tours` (`id`, `full_name`, `cnic`, `mobile`, `email`, `destination`, `room`, `place`, `created_at`, `updated_at`, `tour_form_json`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-20 17:14:00', '2021-10-20 17:14:00', '{\"_token\":\"qz3gjm88prWIIVw0HR8pDeB8D3ehPnX7P7f0QkZ2\",\"text-1634717158065-0\":\"Muhammad Fahad Khan\",\"text-1634717240111\":\"sdfsdfsd\",\"text-1634717250950\":\"sdfsdfds\",\"text-1634717297230\":\"fahad.cs2k16@gmail.com\",\"select-1634717166575-0\":\"Azad Kashmir\",\"select-1634717434311\":\"1 Room (1 bed)\",\"select-1634717535998\":\"Place 1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(4, 'testing 123', '2021-10-18 15:08:25', '2021-10-18 15:22:45'),
(5, 'self', '2021-10-18 15:22:57', '2021-10-18 15:22:57'),
(6, 'popup1', '2021-10-18 16:45:37', '2021-10-18 16:45:37'),
(7, 'Muhammad Fahad Khan', '2021-10-18 16:47:48', '2021-10-18 16:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `isApproved` tinyint(1) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `isApproved`, `comment`, `created_at`, `updated_at`) VALUES
(2, 2, 0, 'dfsdfcssdsdcsddcsd', NULL, '2021-10-18 19:47:40'),
(6, 2, 0, NULL, '2021-10-18 19:48:44', '2021-10-18 19:48:44'),
(7, 2, 1, 'rfgsdfsdfdsfs', '2021-10-18 19:50:24', '2021-10-18 19:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `contact_usdatas`
--

CREATE TABLE `contact_usdatas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `messaage` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_usdatas`
--

INSERT INTO `contact_usdatas` (`id`, `fname`, `lname`, `email`, `messaage`, `created_at`, `updated_at`) VALUES
(3, 'Muhammad', 'Khan', 'fahad.cs2k16@gmail.com', 'asdasdasdasd', '2021-09-30 13:46:13', '2021-09-30 13:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_json` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `form_json`, `form_name`, `created_at`, `updated_at`) VALUES
(1, '[{\"type\":\"text\",\"required\":true,\"label\":\"Full Name\",\"className\":\"form-control\",\"name\":\"text-1634717158065-0\",\"access\":false,\"subtype\":\"text\"},{\"type\":\"text\",\"required\":true,\"label\":\"CNIC\",\"className\":\"form-control\",\"name\":\"text-1634717240111\",\"access\":false,\"subtype\":\"text\"},{\"type\":\"text\",\"subtype\":\"tel\",\"required\":false,\"label\":\"Mobile\",\"className\":\"form-control\",\"name\":\"text-1634717250950\",\"access\":false},{\"type\":\"text\",\"required\":true,\"label\":\"email\",\"className\":\"form-control\",\"name\":\"text-1634717297230\",\"access\":false,\"subtype\":\"text\"},{\"type\":\"select\",\"required\":false,\"label\":\"Destination\",\"className\":\"form-control\",\"name\":\"select-1634717166575-0\",\"access\":false,\"multiple\":false,\"values\":[{\"label\":\"Gilgit\",\"value\":\"Gilgit\",\"selected\":true},{\"label\":\"Azad Kashmir\",\"value\":\"Azad Kashmir\",\"selected\":false},{\"label\":\"Punjab\",\"value\":\"Punjab\",\"selected\":false},{\"label\":\"KPK\",\"value\":\"KPK\",\"selected\":false},{\"label\":\"Islamabad\",\"value\":\"Islamabad\",\"selected\":false}]},{\"type\":\"select\",\"required\":false,\"label\":\"Select Room\",\"className\":\"form-control\",\"name\":\"select-1634717434311\",\"access\":false,\"multiple\":false,\"values\":[{\"label\":\"1 Room (1 bed)\",\"value\":\"1 Room (1 bed)\",\"selected\":true},{\"label\":\"1 Room (2 beds)\",\"value\":\"1 Room (2 beds)\",\"selected\":false},{\"label\":\"2 Rooms (2 beds)\",\"value\":\"2 Rooms (2 beds)\",\"selected\":false},{\"label\":\"2 Rooms (4 beds)\",\"value\":\"2 Rooms (4 beds)\",\"selected\":false},{\"label\":\"others\",\"value\":\"others\",\"selected\":false}]},{\"type\":\"select\",\"required\":false,\"label\":\"Select Place\",\"className\":\"form-control\",\"name\":\"select-1634717535998\",\"access\":false,\"multiple\":false,\"values\":[{\"label\":\"Place 1\",\"value\":\"Place 1\",\"selected\":true},{\"label\":\"Place 2\",\"value\":\"Place 2\",\"selected\":false},{\"label\":\"Place 3\",\"value\":\"Place 3\",\"selected\":false},{\"label\":\"Place 4\",\"value\":\"Place 4\",\"selected\":false}]}]', 'travel_form', '2021-10-20 15:13:02', '2021-10-20 15:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `home_slider_images`
--

CREATE TABLE `home_slider_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_09_29_060242_create_sessions_table', 1),
(7, '2021_09_29_063119_create_pages_table', 2),
(8, '2021_09_29_063211_create_page_metas_table', 2),
(9, '2021_09_29_065524_create_home_slider_images_table', 3),
(10, '2021_09_30_063339_create_contact_usdatas_table', 4),
(11, '2021_09_30_064727_create_quotes_table', 5),
(12, '2021_09_30_065444_create_book_tours_table', 6),
(13, '2021_10_18_075028_create_categories_table', 7),
(14, '2021_10_18_082429_create_tags_table', 8),
(15, '2021_10_18_083810_create_posts_table', 9),
(16, '2021_10_18_094902_create_tag_posts_table', 10),
(17, '2021_10_18_110910_create_comments_table', 11),
(18, '2021_10_20_080159_create_forms_table', 12),
(20, '2021_10_20_100408_add_col_book_tours_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', NULL, NULL),
(2, 'About Us', 'about_us', NULL, NULL),
(3, 'Contact Us', 'contact_us', NULL, NULL),
(4, 'Our Services', 'our_services', NULL, NULL),
(5, 'Our Tour', 'our_tour', NULL, NULL),
(6, 'Travel Form', 'travel_form', NULL, NULL),
(7, 'Get Quotes', 'get_quotes', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_metas`
--

CREATE TABLE `page_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_metas`
--

INSERT INTO `page_metas` (`id`, `page_id`, `type`, `content`, `created_at`, `updated_at`) VALUES
(3, 1, 'why_choose_us', 'afdsdfsdfds', '2021-09-29 14:53:39', '2021-09-29 14:53:39'),
(8, 2, 'about_us_text', 'adasasd', '2021-09-29 16:09:35', '2021-09-29 16:09:35'),
(9, 2, 'about_us_image', 'dining2.jpg_1632906575.jpg', '2021-09-29 16:09:35', '2021-09-29 16:09:35'),
(10, 3, 'contact_number', '+923056641186', '2021-09-29 16:17:06', '2021-09-29 16:17:06'),
(11, 3, 'email_address', 'fahad.cs2k16@gmail.com', '2021-09-29 16:17:06', '2021-09-29 16:17:06'),
(12, 3, 'address', 'aaaa', '2021-09-29 16:17:06', '2021-09-29 16:17:06'),
(13, 4, 'room', 'room', '2021-09-29 16:26:36', '2021-09-29 16:26:36'),
(14, 4, 'night_buffet', 'night', '2021-09-29 16:26:36', '2021-09-29 16:26:36'),
(15, 4, 'cricket', 'cricket', '2021-09-29 16:26:36', '2021-09-29 16:26:36'),
(16, 4, 'destination', 'destination', '2021-09-29 16:26:36', '2021-09-29 16:26:36'),
(17, 4, 'food', 'food', '2021-09-29 16:26:36', '2021-09-29 16:26:36'),
(18, 4, 'bath', 'bath', '2021-09-29 16:26:36', '2021-09-29 16:26:36'),
(19, 5, 'tour_1_text', '1', '2021-09-29 16:41:26', '2021-09-29 16:41:26'),
(20, 5, 'tour_2_text', '2', '2021-09-29 16:41:26', '2021-09-29 16:41:26'),
(21, 5, 'tour_3_text', '3', '2021-09-29 16:41:26', '2021-09-29 16:41:26'),
(22, 5, 'tour_1_image', 'dining2.jpg_1632908486.jpg', '2021-09-29 16:41:26', '2021-09-29 16:41:26'),
(23, 5, 'tour_2_image', 'dining2.jpg_1632908486.jpg', '2021-09-29 16:41:26', '2021-09-29 16:41:26'),
(24, 5, 'tour_3_image', 'dining2.jpg_1632908486.jpg', '2021-09-29 16:41:26', '2021-09-29 16:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `cat_id`, `title`, `slug`, `image`, `content`, `created_at`, `updated_at`) VALUES
(2, 6, 'afddfdsf sfdsfds', 'afddfdsf-sfdsfds', '2940_png-3.png', '<p>sdfdsfsdf</p>', '2021-10-18 17:08:55', '2021-10-18 17:08:55'),
(3, 5, 'thg fgfgfgf', 'thg-fgfgfgf', '8259_png-4.png', '<p>sdfdsfsdf</p>', '2021-10-18 17:10:10', '2021-10-18 17:10:10'),
(4, 6, 'this is sample post', 'this-is-sample-post', '7151_png-2.png', '<p>sddfdfsdf adkfmds sdfsd fsdfsd sdfsdf sdfsdf sdfsd sdf sdf sd fsdfsdf sdfsdf sfsdfsdgsfdsfvfd&nbsp; dfbvvdfvdfvd dfvdfvdvdf ffvffvdfdfsdfsfsdfsdfs</p>', '2021-10-18 18:46:28', '2021-10-18 18:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'fahad.cs2k16@gmail.com', '2021-09-30 13:52:56', '2021-09-30 13:52:56'),
(2, 'fahad.cs2k16@gmail.com', '2021-09-30 13:53:18', '2021-09-30 13:53:18'),
(3, 'matt@bevcoupons.com', '2021-09-30 13:53:27', '2021-09-30 13:53:27'),
(4, 'fahad.cs2k19@gmail.com', '2021-09-30 13:54:06', '2021-09-30 13:54:06');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('Kb1qrILvp8L1nHhE07hqNhNexounWFVph2SSZZ99', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoicXozZ2ptODhwcldJSVZ3MEhSOHBEZUI4RDNlaFBuWDdQN2YwUWtaMiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzk6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9hZG1pbi90cmF2ZWxfZm9ybSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6MzoidXJsIjthOjA6e31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkSm04cjhxRE8uT1hjci9SSVk1Z1BNLnJFcXNSRHQxOVhFWFVSYWphN3ZvVzh0UU00c3VRMEciO30=', 1634726904);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'testing 123', '2021-10-18 15:32:54', '2021-10-18 15:33:33'),
(3, 'Fahad Khan', '2021-10-18 15:33:50', '2021-10-18 15:33:50'),
(4, '123', '2021-10-18 16:47:11', '2021-10-18 16:47:11'),
(5, '4444', '2021-10-18 16:47:41', '2021-10-18 16:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `tag_posts`
--

CREATE TABLE `tag_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tag_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_posts`
--

INSERT INTO `tag_posts` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(7, 2, 2, '2021-10-18 17:08:55', '2021-10-18 17:08:55'),
(8, 2, 3, '2021-10-18 17:08:55', '2021-10-18 17:08:55'),
(9, 3, 4, '2021-10-18 17:10:10', '2021-10-18 17:10:10'),
(10, 3, 5, '2021-10-18 17:10:10', '2021-10-18 17:10:10'),
(11, 4, 3, '2021-10-18 18:46:28', '2021-10-18 18:46:28'),
(12, 4, 4, '2021-10-18 18:46:28', '2021-10-18 18:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Test User', 'test@gmail.com', NULL, '$2y$10$Jm8r8qDO.OXcr/RIY5gPM.rEqsRDt19XEXURaja7voW8tQM4suQ0G', NULL, NULL, NULL, NULL, NULL, '2021-09-29 13:09:15', '2021-09-29 13:09:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_tours`
--
ALTER TABLE `book_tours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `contact_usdatas`
--
ALTER TABLE `contact_usdatas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_slider_images`
--
ALTER TABLE `home_slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_metas`
--
ALTER TABLE `page_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_metas_page_id_foreign` (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_cat_id_foreign` (`cat_id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_posts`
--
ALTER TABLE `tag_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_posts_post_id_foreign` (`post_id`),
  ADD KEY `tag_posts_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_tours`
--
ALTER TABLE `book_tours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `contact_usdatas`
--
ALTER TABLE `contact_usdatas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `home_slider_images`
--
ALTER TABLE `home_slider_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `page_metas`
--
ALTER TABLE `page_metas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tag_posts`
--
ALTER TABLE `tag_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_metas`
--
ALTER TABLE `page_metas`
  ADD CONSTRAINT `page_metas_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tag_posts`
--
ALTER TABLE `tag_posts`
  ADD CONSTRAINT `tag_posts_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_posts_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
