<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    
    public function single_post($id, $slug){
        $post = Post::where('id', $id)->first();
        $comments = Comment::where('post_id', $id)->where('isApproved', 1)->get();
        return view('blog.single_post', compact('post', 'comments'));
    }

    public function comment_store(Request $request){
       
        $comment = new Comment();
        $comment->post_id = $request->post_id;
        $comment->comment = $request->comment;
        $comment->isApproved = false;
        $comment->save();
        $post = Post::where('id', $request->post_id)->first();
        $slug = $post->slug;
        return redirect(url('post/'.$request->post_id.'/'.$slug))->with('success', 'Your Comment is under review');;
    }

    public function all_posts($cate_id = null){
        $Categories = Category::all();
        if($cate_id == null){
            $posts = Post::all();

        }
        else{
            $posts = Post::where('cat_id', $cate_id)->get();
        }

        

        return view('blog.all_posts', compact('posts','Categories','cate_id'));

    }

    public function posts_by_category(Request $request){
        $cate_id = $request->cat_id;
        return redirect(url('all_post/'.$cate_id));
    }
}
