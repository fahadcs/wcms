<?php

namespace App\Http\Controllers;

use App\Models\BookTour;
use App\Models\ContactUsdata;
use App\Models\Form;
use App\Models\HomeSliderImage;
use App\Models\Page;
use App\Models\PageMeta;
use App\Models\quote;
use Illuminate\Http\Request;

class WcmsController extends Controller
{
    public function index(){
        $page = Page::where('slug', 'home')->first();
        $page_meta = PageMeta::where('page_id', $page->id)->get();
        $slider_images = HomeSliderImage::all();
        $why_choose_us = '';
        $why_choose_us_image = '';
        $data = PageMeta::where('page_id', $page->id)->where('type', 'why_choose_us')->first();
        if($data){
            $why_choose_us= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'why_choose_us_image')->first();
        if($data){
            $why_choose_us_image= $data->content;
        }
        if($slider_images->count() > 0){
            echo '<pre>'; print_r($slider_images); exit;
        }
        
        
        return view('front.index', compact('why_choose_us', 'why_choose_us_image','slider_images'));
    }
    public function about_us(){
        $page = Page::where('slug', 'about_us')->first();
        $page_meta = PageMeta::where('page_id', $page->id)->get();
      
        $about_us_text = '';
        $about_us_image = '';
        $data = PageMeta::where('page_id', $page->id)->where('type', 'about_us_text')->first();
        if($data){
            $about_us_text= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'about_us_image')->first();
        if($data){
            $about_us_image= $data->content;
        }
      
        return view('front.about_us', compact('about_us_text','about_us_image'));
    }
    public function contact_us(){
        $page = Page::where('slug', 'contact_us')->first();
        $page_meta = PageMeta::where('page_id', $page->id)->get();
      
        $contact_number = '';
        $email_address = '';
        $address = '';
        $data = PageMeta::where('page_id', $page->id)->where('type', 'contact_number')->first();
        if($data){
            $contact_number= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'email_address')->first();
        if($data){
            $email_address= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'address')->first();
        if($data){
            $address = $data->content;
        }

        
      
        return view('front.contact_us', compact('email_address','address', 'contact_number'));
       
    }
    public function our_services(){
        $page = Page::where('slug', 'our_services')->first();
        $page_meta = PageMeta::where('page_id', $page->id)->get();
      
        $room = '';
        $night_buffet = '';
        $destination = '';
        $bath = '';
        $food = '';
        $cricket = '';
        $data = PageMeta::where('page_id', $page->id)->where('type', 'room')->first();
        if($data){
            $room= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'night_buffet')->first();
        if($data){
            $night_buffet= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'destination')->first();
        if($data){
            $destination = $data->content;
        }
        $data = PageMeta::where('page_id', $page->id)->where('type', 'bath')->first();
        if($data){
            $bath = $data->content;
        }
        $data = PageMeta::where('page_id', $page->id)->where('type', 'food')->first();
        if($data){
            $food = $data->content;
        }
        $data = PageMeta::where('page_id', $page->id)->where('type', 'cricket')->first();
        if($data){
            $cricket = $data->content;
        }
      
        return view('front.our_services', compact('room','night_buffet', 'destination','bath','food', 'cricket'));

    }
    public function get_quotes(){
        return view('front.get_quotes');
    }
    public function our_tour(){
          
        $page = Page::where('slug', 'our_tour')->first();
        $page_meta = PageMeta::where('page_id', $page->id)->get();
      
        $tour_1_text = '';
        $tour_2_text = '';
        $tour_3_text = '';
        $tour_1_image = '';
        $tour_2_image = '';
        $tour_3_image = '';
        $data = PageMeta::where('page_id', $page->id)->where('type', 'tour_1_text')->first();
        if($data){
            $tour_1_text= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'tour_2_text')->first();
        if($data){
            $tour_2_text= $data->content;
        }

        $data = PageMeta::where('page_id', $page->id)->where('type', 'tour_3_text')->first();
        if($data){
            $tour_3_text = $data->content;
        }
        $data = PageMeta::where('page_id', $page->id)->where('type', 'tour_1_image')->first();
        if($data){
            $tour_1_image = $data->content;
        }
        $data = PageMeta::where('page_id', $page->id)->where('type', 'tour_2_image')->first();
        if($data){
            $tour_2_image = $data->content;
        }
        $data = PageMeta::where('page_id', $page->id)->where('type', 'tour_3_image')->first();
        if($data){
            $tour_3_image = $data->content;
        }
      
        return view('front.our_tour', compact('tour_1_text','tour_2_text', 'tour_3_text','tour_1_image','tour_2_image', 'tour_3_image'));
        
    }
    public function travel_form(){
        return view('front.travel_form');
    }

    public function get_travel_form(Request $request){
        $form = Form::where('form_name', 'travel_form')->first();
        if($form){
            return $form->form_json;
        }
    }


    public function contact_us_store(Request $request){
        $validated = $request->validate([

            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'message' => 'required',

        ]);

        $data = new ContactUsdata();
        $data->fname = $request->fname;
        $data->lname = $request->lname;
        $data->email = $request->email;
        $data->messaage = $request->message;
        $data->save();

        return redirect(route('contact_us'))->with('message', 'Thank you, Your Message has been sent' );


    }

    public function quotes_store(Request $request){
        $validated = $request->validate([
            'email' => 'required|email',
        ]);

        $data = new quote();
        $data->email = $request->email;
        $data->save();

        return redirect(route('get_quotes'))->with('message', 'Thank you for subscribe for updates' );

    }

    public function book_tour(Request $request){
    
        //echo '<pre>'; print_r($request->all()); exit;
        $json_data = json_encode($request->all());
        $data = new BookTour(); 
        $data->tour_form_json = $json_data;
        $data->save();

        return redirect(route('travel_form'))->with('message', 'Thank you for booking a tour.' );
    }
}
