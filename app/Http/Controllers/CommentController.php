<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $PostComment = Comment::where('isApproved' , '=' , '0')->get();
        $PostCommentApproved = Comment::where('isApproved' , '=' , '1')->get();
        return view('admin.post.comments' , compact('PostComment' , 'PostCommentApproved'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function ApproveComment($id){


        $PostComment = Comment::where('id' , $id)->first();
        $PostComment->isApproved = 1;

        $PostComment->save();

     }


     public function DisApproveComment($id){


        $PostComment = Comment::where('id' , $id)->first();
        $PostComment->isApproved = 0;

        $PostComment->save();

     }









    public function DelApproveComment($id)
    {
        $makeModel=Comment::where('id', $id)->delete();
        echo "deleted";
    }
}
