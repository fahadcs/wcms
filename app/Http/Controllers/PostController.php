<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\TagPost;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Posts = Post::all();
        $Content = Post::pluck('content');
        return view('admin.post.index', compact('Posts', 'Content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Categories = Category::all();
        $Tags = Tag::all();
        return View('admin.post.create' , compact('Categories' , 'Tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([

            'title' => 'required',
            'slug' => 'required',
            'cat_id' => 'required',
            'content' => 'required',
            'tags' => 'required',


        ]);




        if($request->post_id != ''){
            $Post = Post::where('id', $request->post_id)->first();

            $image = $request->file('image');

            if($image){

            $imageName = $image->getClientOriginalName();

            $image->move(public_path('images/post'), $imageName);
           \File::delete(public_path('images/post/'.$Post->image));


            $Post->image = $imageName;
            $Post->title = $request->title;
            $Post->slug = $request->slug;
            $Post->cat_id = $request->cat_id;
          //  $Post->tags = $request->tags;
            $Post->content = $request->content;


            $Post->save();
            $Post->id;

            $TagPost = TagPost::where('post_id', $request->post_id)->delete();
            foreach($request->tags as $item){

                $TagPost = new TagPost();
                $TagPost->tag_id = $item;
                $TagPost->post_id =$Post->id;

                 $TagPost->save();
               }



            return redirect(route('admin.post.index'));
            }
            else{

              
                $Post->title = $request->title;
                $Post->slug = $request->slug;
                $Post->cat_id = $request->cat_id;
              //  $Post->tags = $request->tags;
                $Post->content = $request->content;

                $Post->save();
                $Post->id;

                $TagPost = TagPost::where('post_id', $request->post_id)->delete();
                foreach($request->tags as $item){

                    $TagPost = new TagPost();
                    $TagPost->tag_id = $item;
                    $TagPost->post_id =$Post->id;

                     $TagPost->save();
                   }
                return redirect(route('post'));

            }


           }













        else{



        $Post = new Post();
        $Post->title = $request->title;
        $Post->slug = $request->slug;
        $Post->cat_id = $request->cat_id;
        $Post->content = $request->content;



        if ($request->hasFile('image')) {
            $feature_image = $request->file('image');
            $name = rand(100,10000).'_'.$feature_image->getClientOriginalName();

           // $feature_image->move(public_path('/section-images/home'),$name);
            $feature_image->move(public_path('images/post'), $name);
            $Post->	image = $name;
        }
        $Post->save();
        $Post->id;



         foreach($request->tags as $item){

            $TagPost = new TagPost();
            $TagPost->tag_id = $item;
            $TagPost->post_id =$Post->id;

             $TagPost->save();
           }






    }

       return redirect(route('admin.post.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Post = Post::where('id' , $id)->first();
        $TagPost = TagPost::where('post_id', $id)->pluck('tag_id')->toArray();
        $Categories = Category::all();
        $Tags = Tag::all();
        return View('admin.post.create' , compact('Post' , 'Categories','TagPost','Tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Post=Post::where('id', $id)->first();
        \File::delete(public_path('images/post/'.$Post->image));

        $Post=Post::where('id', $id)->delete();
        echo true;

    }
}
