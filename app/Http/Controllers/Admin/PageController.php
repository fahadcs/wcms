<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomeSliderImage;
use App\Models\Page;
use App\Models\PageMeta;
use Illuminate\Http\Request;
use Auth;

class PageController extends Controller
{
    public function home_store(Request $request){
        $page = Page::where('slug', 'home')->first();

        PageMeta::where('page_id', $page->id)->delete();
        $page_meta = new PageMeta();
        $page_meta->type = 'why_choose_us';
        $page_meta->content = $request->why_choose_us;
        $page_meta->page_id = $page->id;
        $page_meta->save();


        $file = $request->file('why_choose_us_image');
            if ($file) {
            $extension = $file->getClientOriginalExtension();
            $fileName = $file->getClientOriginalName().'_'.time().'.'.$extension;
            $file->move(public_path('home/'), $fileName);

            }

        $page_meta = new PageMeta();
        $page_meta->type = 'why_choose_us_image';
        $page_meta->content = $fileName;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        return redirect(route('admin.dashboard'));
        

    }

    public function about_us_store(Request $request){
        $page = Page::where('slug', 'about_us')->first();

        PageMeta::where('page_id', $page->id)->delete();
        $page_meta = new PageMeta();
        $page_meta->type = 'about_us_text';
        $page_meta->content = $request->about_us_text;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $fileName= '';
        $file = $request->file('about_us_image');
            if ($file) {
            $extension = $file->getClientOriginalExtension();
            $fileName = $file->getClientOriginalName().'_'.time().'.'.$extension;
            $file->move(public_path('about_us/'), $fileName);

            }

        $page_meta = new PageMeta();
        $page_meta->type = 'about_us_image';
        $page_meta->content = $fileName;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        return redirect(route('admin.about_us'));
        
    }


    public function contact_us_store(Request $request){
        $page = Page::where('slug', 'contact_us')->first();

        PageMeta::where('page_id', $page->id)->delete();
        $page_meta = new PageMeta();
        $page_meta->type = 'contact_number';
        $page_meta->content = $request->contact_number;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'email_address';
        $page_meta->content = $request->email_address;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'address';
        $page_meta->content = $request->address;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        

        return redirect(route('admin.contact_us'));
        
    }


    public function our_services_store(Request $request){
        $page = Page::where('slug', 'our_services')->first();

        PageMeta::where('page_id', $page->id)->delete();
        $page_meta = new PageMeta();
        $page_meta->type = 'room';
        $page_meta->content = $request->room;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'night_buffet';
        $page_meta->content = $request->night_buffet;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'cricket';
        $page_meta->content = $request->cricket;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'destination';
        $page_meta->content = $request->destination;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'food';
        $page_meta->content = $request->food;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'bath';
        $page_meta->content = $request->bath;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        

        return redirect(route('admin.our_services'));
        
    }


    public function our_tour_store(Request $request){
        $page = Page::where('slug', 'our_tour')->first();

        PageMeta::where('page_id', $page->id)->delete();
        $page_meta = new PageMeta();
        $page_meta->type = 'tour_1_text';
        $page_meta->content = $request->tour_1_text;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'tour_2_text';
        $page_meta->content = $request->tour_2_text;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $page_meta = new PageMeta();
        $page_meta->type = 'tour_3_text';
        $page_meta->content = $request->tour_3_text;
        $page_meta->page_id = $page->id;
        $page_meta->save();


        $tour_1_image= '';
        $file = $request->file('tour_1_image');
            if ($file) {
            $extension = $file->getClientOriginalExtension();
            $tour_1_image = $file->getClientOriginalName().'_'.time().'.'.$extension;
            $file->move(public_path('tour/'), $tour_1_image);

            }

        $page_meta = new PageMeta();
        $page_meta->type = 'tour_1_image';
        $page_meta->content = $tour_1_image;
        $page_meta->page_id = $page->id;
        $page_meta->save();

        $tour_2_image= '';
        $file = $request->file('tour_2_image');
            if ($file) {
            $extension = $file->getClientOriginalExtension();
            $tour_2_image = $file->getClientOriginalName().'_'.time().'.'.$extension;
            $file->move(public_path('tour/'), $tour_2_image);

            }

        $page_meta = new PageMeta();
        $page_meta->type = 'tour_2_image';
        $page_meta->content = $tour_2_image;
        $page_meta->page_id = $page->id;
        $page_meta->save();


        $tour_3_image= '';
        $file = $request->file('tour_3_image');
            if ($file) {
            $extension = $file->getClientOriginalExtension();
            $tour_3_image = $file->getClientOriginalName().'_'.time().'.'.$extension;
            $file->move(public_path('tour/'), $tour_3_image);

            }

        $page_meta = new PageMeta();
        $page_meta->type = 'tour_3_image';
        $page_meta->content = $tour_3_image;
        $page_meta->page_id = $page->id;
        $page_meta->save();
        
        

        return redirect(route('admin.our_tour'));
        
    }


    

    public function slider_images_upload(Request $request){
        
            $file = $request->file('slider_image');
            if ($file) {
            $extension = $file->getClientOriginalExtension();
            $fileName = $file->getClientOriginalName().'_'.time().'.'.$extension;
            $file->move(public_path('home/slider_images/'), $fileName);

            
            $slider_image = new HomeSliderImage();
            $slider_image->image =  $fileName;
            $slider_image->save();
        }
        return response()->json(['success' => true, 'id' => $slider_image->id]);
        
        
    }


    public function delete_slider_image(Request $request){
          
        $id = $request->get('id');
       
        $image = HomeSliderImage::find($id);
        // $customer = Customer::find($image->customer_id);
        \File::delete(public_path('home/slider_images/'.$image->image));

        $image->delete();


        return response()->json(['success'=> true]);
    }
}
