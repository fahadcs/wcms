@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->
<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a target="_blank" style="float: right;" href="{{route('contact_us')}}" class="btn btn-light">View</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">WMCS Tours</h2>
    <div class="separator"></div>
<form action="{{route('admin.contact_us_store')}}" method="POST" enctype="multipart/form-data" class="mb-5">
      @csrf
<div class="row text-white mb-0">
    <div class="col-md-4">
        <h2>Contact Number</h2>
        <input class="form-control" type="text" placeholder="Enter Contact number" name="contact_number" id="contact_number" value="{{$contact_number}}">
    </div>
    <div class="col-md-4">
        <h2>Email Address</h2>
        <input class="form-control" type="text" placeholder="Enter Contact number" name="email_address" value="{{$email_address}}">
    </div>
    <div class="col-md-4">
        <h2>Address</h2>
        <input class="form-control" type="text" placeholder="Enter Contact number" name="address" value="{{$address}}">
    </div>
    <button type="submit" class="mt-5 btn btn-light" style="width: 100%;">Save</button>


</div>
</form>

<table class="table text-white" id="contact_table">
<thead>
  <tr>
    <th scope="col">#</th>
    <th scope="col">User First Name</th>
    <th scope="col">User Last Name</th>
    <th scope="col">User Email</th>
    <th scope="col">User Message</th>
  </tr>
</thead>
<tbody>
  @foreach ($contact_us_data as  $data)
  <tr>
    <th>{{$data->id}}</th>
    <td>{{$data->fname}}</td>
    <td>{{$data->lname}}</td>
    <td>{{$data->email}}</td>
    <td>{{$data->messaage}}</td>
  </tr>
  @endforeach
  
</tbody>
</table>

</div>

<script>
  $(document).ready( function () {
  $(function(){
    $("#contact_table").dataTable();
  });

  });
</script>
@endsection