@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->


    <div class="page-content p-5" id="content">
        <!-- Toggle Navbar button -->
        <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
                class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
                <a target="_blank" style="float: right;" href="{{route('our_tour')}}" class="btn btn-light">View</a>
        <!-- WCMS content -->
        <form action="{{route('admin.our_tour_store')}}" method="POST" enctype="multipart/form-data" class="mb-5">
          @csrf
        <h2 class="display-3 text-white">WMCS Tours</h2>
        <div class="separator"></div>
        <div class="form-group">
            <h2 class="mb-3 text-white">Tours Text 1</h2>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="tour_1_text">{{$tour_1_text}}</textarea>
          </div>
            <div class="form-group text-white">
                <!-- About Us Image-->
                <h2 class="mt-3">Tours Image 1</h2>
                <input type="file" class="form-control-file mb-5 mt-5" id="exampleFormControlFile1" name="tour_1_image">
                @if($tour_1_image != '')
                  <img src="{{ asset('tour/'.$tour_1_image.'')}}" class="sliderImages" style="width: 150px" />
                @endif
        </div>
        <div class="form-group">
            <h2 class="mb-3 text-white">Tours Text 2</h2>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="tour_2_text">{{$tour_2_text}}</textarea>
          </div>
            <div class="form-group text-white">
                <!-- About Us Image-->
                <h2 class="mt-3">Tours Image 2</h2>
                <input type="file" class="form-control-file mb-5 mt-5" id="exampleFormControlFile1"  name="tour_2_image">
                @if($tour_2_image != '')
                <img src="{{ asset('tour/'.$tour_2_image.'')}}" class="sliderImages" style="width: 150px" />
              @endif
              </div>
        <div class="form-group">
            <h2 class="mb-3 text-white">Tours Text 3</h2>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="tour_3_text">{{$tour_3_text}}</textarea>
          </div>
            <div class="form-group text-white">
                <!-- About Us Image-->
                <h2 class="mt-3">Tours Image 3</h2>
                <input type="file" class="form-control-file mb-5 mt-5" id="exampleFormControlFile1"  name="tour_3_image">
                @if($tour_3_image != '')
                <img src="{{ asset('tour/'.$tour_3_image.'')}}" class="sliderImages" style="width: 150px" />
                @endif
              </div>
        <button type="submit" class="mt-5 btn btn-light" style="width: 100%;">Save</button>
        </form>
            </div>


@endsection