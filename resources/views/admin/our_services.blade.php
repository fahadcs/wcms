@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->
<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a target="_blank" style="float: right;" href="{{route('our_services')}}" class="btn btn-light">View</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">WMCS Services</h2>
    <div class="separator"></div>
    <!-- Edit Content-->
    <form action="{{route('admin.our_services_store')}}" method="POST" enctype="multipart/form-data" class="mb-5">
        @csrf
    <div class="row mt-2 w-100 text-white">
        <div class="col-md-12">
            <div class="form-group">
                <h2 class="mb-3 text-white">Clean Room Text</h2>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="room">{{$room}}</textarea>
              </div>
        </div>
    </div>
    <div class="row mt-2 w-100 text-white">
        <div class="col-md-12">
            <div class="form-group">
                <h2 class="mb-3 text-white">Clean Night Buffet</h2>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="night_buffet">{{$night_buffet}}</textarea>
              </div>
        </div>
        <!-- End Edit Content-->
    </div>
    <div class="row mt-2 w-100 text-white">
        <div class="col-md-12">
            <div class="form-group">
                <h2 class="mb-3 text-white">Edit Cricket</h2>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="cricket">{{$cricket}}</textarea>
              </div>
        </div>
    </div>
    <div class="row mt-2 w-100 text-white">
        <div class="col-md-12">
            <div class="form-group">
                <h2 class="mb-3 text-white">Edit Destinations</h2>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="destination">{{$destination}}</textarea>
              </div>
        </div>
    </div>
    <div class="row mt-2 w-100 text-white">
        <div class="col-md-12">
            <div class="form-group">
                <h2 class="mb-3 text-white">Edit Food</h2>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="food">{{$food}}</textarea>
              </div>
        </div>
    </div>
    <div class="row mt-2 w-100 text-white">
        <div class="col-md-12">
            <div class="form-group">
                <h2 class="mb-3 text-white">Edit Bath</h2>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="bath">{{$bath}}</textarea>
              </div>
        </div>
        <button type="submit" class="mt-5 btn btn-light" style="width: 100%;">Save</button>

    </div>
    </form>
</div>
@endsection