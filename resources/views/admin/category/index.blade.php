@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->
<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a  style="float: right;" href="{{route('admin.category.create')}}" class="btn btn-light">+ Add New</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">Categories</h2>
    <div class="separator"></div>


<table class="table text-white" id="contact_table">
<thead>
  <tr>
    <th scope="col">#</th>
    <th scope="col">Name</th>
    <th scope="col">Action</th>
  </tr>
</thead>
<tbody>
  @foreach ($Category as  $data)
  <tr>
    <th>{{$data->id}}</th>
    <td>{{$data->name}}</td>
    <td>
        <a class="btn btn-warning" href="{{ route('admin.category.edit',$data->id) }}">Edit</a>
        {{-- <a class="btn btn-danger" href="{{ route('DelCat' ,$cat->id) }}">Delete</a> --}}
        <a href="javascript:" data-id="{{ $data->id }}" class="delete btn btn-danger ">Delete</a>
    </td>
  </tr>
  @endforeach
  
</tbody>
</table>

</div>

<script>
  $(document).ready( function () {
  $(function(){
    $("#contact_table").dataTable();
  });

  });
</script>
<script>

    $(document).on('click', '.delete', function(){
               var id = $(this).data('id');
           //   alert(id);
               swal({
                   title: "Are you sure?",
                   text: "Once deleted, you will not be able to recover data!",
                   icon: "warning",
                   buttons: true,
                   dangerMode: true,
                   })
                 .then((willDelete) => {
                 if (willDelete) {
                       var url = "{{route('admin.category.destroy', ':id')}}";
                       url = url.replace(':id', id);

                       var token = "{{ csrf_token() }}";

                       $.ajax({
                           type: 'POST',
                               url: url,
                               data: {'_token': token, '_method': 'DELETE'},
                               success: function (response) {
                               window.location.reload();
                           }
                       });

                  }

                  else {
                         swal("Your imaginary file is safe!");
                       }
             });





                    });

</script>
@endsection