@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->

<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
    <!-- WCMS content -->
    <form action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data">
      @csrf
    <h2 class="display-3 text-white">Add New Category</h2>
    <div class="separator"></div>
    <div class="form-group">
        <h2 class="mb-3 text-white">Name</h2>
        <input type="hidden" name="cat_id" id="cat_id" value="@if(isset($CategoryEdit)) {{$CategoryEdit->id}} @endif" >
        <input class="form-control" name="name" id="name" type="text" value="@if(isset($CategoryEdit)) {{$CategoryEdit->name}} @endif"  required >
      </div>
        
    <button type="submit" class="mt-5 btn btn-light" style="width: 100%;">Save</button>
    </form>
</div>
@endsection