@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->

<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a target="_blank" style="float: right;" href="{{route('about_us')}}" class="btn btn-light">View</a>
    <!-- WCMS content -->
    <form action="{{route('admin.about_us_store')}}" method="POST" enctype="multipart/form-data">
      @csrf
    <h2 class="display-3 text-white">WMCS About Us</h2>
    <div class="separator"></div>
    <div class="form-group">
        <h2 class="mb-3 text-white">About us Text</h2>
        <textarea class="form-control" id="about_us_text" name="about_us_text" rows="3">{{$about_us_text}}</textarea>
      </div>
        <div class="form-group text-white">
            <!-- About Us Image-->
            <h2 class="mt-3">About Us Image</h2>
            <input type="file" class="form-control-file mb-5 mt-5" id="about_us_image" name="about_us_image">
            @if($about_us_image != '')
              <img src="{{ asset('about_us/'.$about_us_image.'')}}" class="sliderImages" style="width: 150px" />
            @endif
    </div>
    <button type="submit" class="mt-5 btn btn-light" style="width: 100%;">Save</button>
    </form>
</div>
@endsection