<!-- Modal -->
<div class="modal fade" id="TagModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Tag</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">





                <form id="tag_form"  action="{{ route('admin.tag_popup') }}" method="POST" enctype="multipart/form-data">

                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fname">Tag Name</label>
                                <input type="text" class="form-control"  id="name" name="name" >
                            </div>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>




                    {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                  {{-- </form> --}}






        </div>
        <div class="modal-footer">
          <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button id="submitTag" type="submit" class="btn btn-primary" >Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>

  <script>

    $('#submitTag').click(function(e){
   e.preventDefault();
   /*Ajax Request Header setup*/
  //  alert('submitTag');
   // var a =  $('#password').val();
   // alert(a);
   $.ajaxSetup({
       headers: {
           "_token": "{{ csrf_token() }}",
       }
   });


   $.ajax({
       url: "{{ route('admin.tag_popup')}}",
       method: 'post',
       data: $('#tag_form').serialize(),

       success: function (data) {
               // console.log(data);
       $("#TagModal #close").click();
       $('#tag_form').trigger("reset");
               var s = '';
               for (var i = 0; i < data.length; i++) {
                   s += '<option value="' + data[i].id + '" >' + data[i].name + '</option>';

               }
               $("#tags_id").html(s);



           }









    });


       });


</script>


