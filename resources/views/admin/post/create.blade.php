@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->

<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
    <!-- WCMS content -->
    <form action="{{route('admin.post.store')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="post_id" value="@if(isset($Post)){{$Post->id}} @endif" >
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="fname">Title :</label>
                    <input type="text" class="form-control" id="title" name="title" value="@if(isset($Post)){{$Post->title}} @endif">
                </div>
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="fname">Slug :</label>
                    <input type="text" class="form-control" id="slug" name="slug" value="@if(isset($Post)){{$Post->slug}} @endif">
                </div>
                @error('slug')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>
        @if(isset($Post))
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="fname">old Image :</label>
                  <img class=" ml-5" src="{{ asset('images/post/'.$Post->image)}}"  width="250" height="100">
                </div>
                @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="fname">Feature Image :</label>
                   <input type="file" name = "image" class="form-control " value="@if(isset($Post)){{$Post->image}} @endif">
                </div>
                @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>



        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="fname">Category :</label></br>
                    <select class="form-control" type="text" name="cat_id" id="cat_id" >
                       <option value="">Select Category</option>

                     @foreach ($Categories as $cat)
                            {{-- <option value="{{$cat->id}}">{{$cat->name}}</option> --}}
 <option class="option" @if(isset($Post)) @if($Post->cat_id  == $cat->id) selected @endif  @endif value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                </div>
                    @error('cat_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>


            <div class="col-md-4 mt-2">
               <div class="form-group">
                <label for="fname"></label></br>
                  <a class="btn btn-primary" href="javascript:" class="show " id="cat" data-toggle="modal" data-target="#exampleModal">Add Category</a>
               </div>
            </div>



        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="fname">Tags :</label></br>
                    <select class="form-control" type="text" name="tags[]" class="tags" id="tags_id" multiple>

                        @foreach ($Tags as $tag)
                        {{-- <option value="{{$model->name}}">{{$model->name}}</option> --}}

                        <option @if(isset($TagPost)) @if(in_array($tag->id , $TagPost)) selected @endif @endif value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                    </select>
                </div>
                    @error('tags')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>


            <div class="col-md-4">
                <div class="form-group">
                    <label for="fname"></label></br>
                    {{-- <a type="button" class="btn btn-primary mt-2 text-white" style="float: left;" data-toggle="modal" data-target="TagModal">+ Add Tag</a> --}}
                    <a class="btn btn-primary" href="javascript:" class="show " id="cat" data-toggle="modal" data-target="#TagModal">Add Tag</a>
                </div>

            </div>


        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label><strong>Description :</strong></label>
                    <textarea class="form-control" id="editor1" name="content">@if(isset($Post)){{$Post->content}} @endif</textarea>
                </div>

            </div>
        </div>
    <button type="submit" class="mt-5 btn btn-light" style="width: 100%;">Save</button>
    </form>
</div>
@include('admin.post.category_popup')
@include('admin.post.tag_popup')

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">
    CKEDITOR.replace( 'editor1' );
</script>
<script>
    $(document).ready(function(){
   $("#title").on("keyup", function () {
       var Text = $(this).val();
           Text = Text.toLowerCase();
           Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
      $("#slug").val(Text);
   });


    });
   </script>


@endsection