@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->
<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a  style="float: right;" href="{{route('admin.post.create')}}" class="btn btn-light">+ Add New</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">Posts</h2>
    <div class="separator"></div>


<table class="table text-white" id="contact_table">
<thead>
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Slug</th>
        <th>feature Image</th>
        <th>Category</th>
        <th width="200px">Action</th>
    </tr>
</thead>
<tbody>
    @foreach ($Posts as $post)
    <tr>
        <td>{{ $post->id }}</td>
        <td>{{ $post->title }}</td>
        <td>{{ $post->slug  }}</td>
        {{-- <td>{{ $post->image }}</td> --}}
        <td><img src="{{ asset('images/post/'.$post->image)}}"  width="200" height="100"></td>
        <td>{{ $post->Category->name }}</td>
       
        <td>
           @csrf
           <a class="btn btn-warning" href="{{ route('admin.post.edit',$post->id) }}">Edit</a>
           <a class="btn btn-danger delete" data-id="{{ $post->id }}" href="javascript:">Delete</a>
           {{-- <a href="javascript:" data-id="{{ $post->id }}" class="delete btn btn-danger ">Delete Post</a> --}}
        </td>

    </tr>
    @endforeach

  
</tbody>
</table>

</div>

<script>
  $(document).ready( function () {
  $(function(){
    $("#contact_table").dataTable();
  });

  });
</script>
<script>

    $(document).on('click', '.delete', function(){
               var id = $(this).data('id');
           //   alert(id);
               swal({
                   title: "Are you sure?",
                   text: "Once deleted, you will not be able to recover data!",
                   icon: "warning",
                   buttons: true,
                   dangerMode: true,
                   })
                 .then((willDelete) => {
                 if (willDelete) {
                       var url = "{{route('admin.post.destroy', ':id')}}";
                       url = url.replace(':id', id);

                       var token = "{{ csrf_token() }}";

                       $.ajax({
                           type: 'POST',
                               url: url,
                               data: {'_token': token, '_method': 'DELETE'},
                               success: function (response) {
                               window.location.reload();
                           }
                       });

                  }

                  else {
                         swal("Your imaginary file is safe!");
                       }
             });





                    });

</script>
@endsection