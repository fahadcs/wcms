<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Category</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">





            <form role="form" id="add_name" method="post" action="{{ route('admin.cat_popup') }}">
                   
                    @csrf



                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fname">Category Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>




                 




        </div>
        <div class="modal-footer">
          <button type="button" id="close" class="btn btn-secondary " data-dismiss="modal">Close</button>
          <button id="submit" type="submit" class="btn btn-primary cat" >Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>





<script>

    $('#submit').click(function(e){
   e.preventDefault();
   /*Ajax Request Header setup*/
 //   alert('hash');
   // var a =  $('#password').val();
   // alert(a);
   $.ajaxSetup({
       headers: {
           "_token": "{{ csrf_token() }}",
       }
   });


   $.ajax({
       url: "{{ route('admin.cat_popup')}}",
       method: 'post',
       data: $('#add_name').serialize(),

       success: function (data) {

       $("#exampleModal #close").click();
       $('#add_name').trigger("reset");
               var s = '<option value="-1"> Select Category</option>';
               for (var i = 0; i < data.length; i++) {
                   s += '<option value="' + data[i].id + '" selected>' + data[i].name + '</option>';

               }
               $("#cat_id").html(s);
           }









    });


       });


</script>
