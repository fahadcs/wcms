@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->
<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">Comments</h2>
    <div class="separator"></div>


    <table class="table table-bordered data-table" id="comment_table">
        <thead>
            <tr>
                <th>S No.</th>
                <th>Post id</th>
                <th>Comments</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>


                @foreach ($PostComment as $post)
                 <tr>
                     <td>{{ $post->id }}</td>
                     <td>{{ $post->post_id  }}</td>
                     <td>{{ $post->comment }}</td>
                     

                     <td>
                         <a href="javascript:" data-id="{{ $post->id }}" class="approve btn btn-success ">Aprrove</a>
                         <a href="javascript:" data-id="{{ $post->id }}" class="delete btn btn-danger ">Delete</a>
                        {{-- <span>
                            <a  href="javascript:" data-id="{{ $post->id }}" class="disapprove btn btn-danger" href="">Dis-Approve</a>
                        </span> --}}
                     </td>


                 </tr>
                @endforeach


                @foreach ($PostCommentApproved as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->post_id  }}</td>
                    <td>{{ $post->comment }}</td>
                   

                    <td>
                        {{-- <a href="javascript:" data-id="{{ $post->id }}" class="approve btn btn-warning ">Aprrove</a> --}}
                       <span>
                           <a  href="javascript:" data-id="{{ $post->id }}" class="disapprove btn btn-danger" href="">un-Approve</a>

                       </span>
                    </td>


                </tr>
               @endforeach

        </tbody>
    </table>





</div>

<script>
  $(document).ready( function () {
  $(function(){
    $("#comment_table").dataTable();
  });

  });
</script>

<script>
    $(document).on('click', '.check', function(){
       swal("Hello world!");
   });
           $(document).on('click', '.approve', function(){
              var id = $(this).data('id');
           //  alert(id);
              swal({
                  title: "Are you sure?",
                  text: "Comment will Display on The Live Post!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  })
                .then((willDelete) => {
                if (willDelete) {
                      var url = "{{ route('admin.ApproveComment',':id') }}";
                      url = url.replace(':id', id);

                      var token = "{{ csrf_token() }}";

                      $.ajax({
                          type: 'GET',
                              url: url,
                              data: {'_token': token, '_method': 'DELETE'},
                              success: function (response) {

                                location.reload();

                          }
                      });

                 }

            });



                   });



                   $(document).on('click', '.disapprove', function(){
              var id = $(this).data('id');
           //  alert(id);
              swal({
                  title: "Are you sure?",
                  text: "Comment will Not Display on The Live Post......!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  })
                .then((willDelete) => {
                if (willDelete) {
                      var url = "{{ route('admin.DisApproveComment',':id') }}";
                      url = url.replace(':id', id);

                      var token = "{{ csrf_token() }}";

                      $.ajax({
                          type: 'GET',
                              url: url,
                              data: {'_token': token, '_method': 'DELETE'},
                              success: function (response) {
                              //   alert('Comment Dis-Approved on the post');


                              location.reload();
                           // loadTable();



                          }
                      });

                 }



             });




                   });




                   $(document).on('click', '.delete', function(){
              var id = $(this).data('id');
          //   alert(id);
              swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                  })
                .then((willDelete) => {
                if (willDelete) {
                      var url = "{{ route('admin.DelApproveComment',':id') }}";
                      url = url.replace(':id', id);

                      var token = "{{ csrf_token() }}";

                      $.ajax({
                          type: 'GET',
                              url: url,
                              data: {'_token': token, '_method': 'DELETE'},
                              success: function (response) {
                               swal("Poof! Your imaginary file has been deleted!", {
                                      icon: "success",
                                            });
                            location.reload();
                          

                          }
                      });

                 }

                 else {
                        swal("Your imaginary file is safe!");
                      }
            });





                   });







</script>

@endsection