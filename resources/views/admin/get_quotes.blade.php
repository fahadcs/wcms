@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->

<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a target="_blank" style="float: right;" href="{{route('get_quotes')}}" class="btn btn-light">View</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">WMCS Subscribed Email List</h2>
    <table class="table text-white" id="quote_table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Emails</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($quotes as $quote)
            <tr>
                <th>{{$quote->id}}</th>
                <td>{{$quote->email}}</td>
            </tr>
            @endforeach
          
          
        </tbody>
        </table>
        
    <div class="separator"></div>

    

    </div>

    <script>
        $(document).ready( function () {
        $(function(){
          $("#quote_table").dataTable();
        });

        });
    </script>
@endsection