@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->

<div class="page-content p-5" id="content">
    <!-- Toggle Navbar button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
            class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
            <a target="_blank" style="float: right;" href="{{route('travel_form')}}" class="btn btn-light">View</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">WMCS Tours Applications</h2>
    <div class="separator"></div>
    <div id="fb-editor"></div>
    <h4 class="text-white">Existing Table</h4>
    <div class="mt-3 mb-5" id="render-wrap"></div>
    <h4 class="text-white">Data From Frontend</h4>
    <table class="table text-white table-bordered" id="bookings" >
        
        <tbody>
          @foreach ($bookings as  $data)
          <tr>
            <th scope="row">{{$data->id}}</th>
            @php
               $data = json_decode($data->tour_form_json,true);
               $i = 0; 
            @endphp
          @foreach($data as $key => $value)
            @if($key == '_token')
              @continue
            @endif
           <td>{{$value}}</td>  
          @endforeach 
          
          </tr>
          @endforeach
          
        </tbody>
      </table>
      
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>
  <script src="https://formbuilder.online/assets/js/form-render.min.js"></script>
  <script>
    var options = {
      onSave: function(evt, formData) {
        
          console.log(formData);
        
          var url = "{{ route('admin.store_travel_form') }}";
          var token = "{{ csrf_token() }}";

                       $.ajax({
                           type: 'POST',
                               url: url,
                               data: {'_token': token, 'formData': formData},
                               success: function (response) {
                                 swal("Form Save Successfully");
                                 window.location.reload();

                           }
                       });
        },
    };
  $('#fb-editor').formBuilder(options);
  

  
</script>
<script>
  $(document).ready( function () {
  $(function(){
    $("#bookings").dataTable();
  });

  var url = "{{ route('get_travel_form') }}";
         
                       $.ajax({
                           type: 'GET',
                               url: url,
                               
                               success: function (response) {
                                /* console.log(typeof(response));
                                console.log(response);
                                $(".render-wrap").formRender({response}); */
                                const code = document.getElementById("render-wrap");
                                const formData = response;
                                const addLineBreaks = html => html.replace(new RegExp("><", "g"), ">\n<");

                                // Grab markup and escape it
                                const $markup = $("<div/>");
                                $markup.formRender({ formData });

                                // set < code > innerText with escaped markup
                                $("#render-wrap").html(addLineBreaks($markup.formRender("html")));

                           }
  });

  });
</script>
@endsection