@extends('layout.admin_app')
@section('content')

<!-- Page content holder -->
<div class="page-content p-5" id="content">
    <!-- Toggle button -->
    <button id="sidebarCollapse" type="button" class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4"><i
        class="fa fa-bars mr-2"></i><small class="text-uppercase font-weight-bold">Toggle</small></button>
        <a target="_blank" style="float: right;" href="{{route('index')}}" class="btn btn-light">View</a>
    <!-- WCMS content -->
    <h2 class="display-3 text-white">WCMS Editor</h2>
    
    <div class="separator"></div>
<form action="{{route('admin.home_store')}}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="container-fluid">
    <div class="form-group  text-white">
      <!-- Select Carasoul Images-->  
      <h2 class="mt-3">Select Carasoul Images</h2>
      <input type="file" class="form-control-file mb-5 mt-5" id="slider_images" name="slider_images" accept="image/jpg, image/jpeg, image/gif, image/png" multiple>
  </div>
  <div class="row" id="sliderImages">
    @if(isset($slider_images))
    @foreach($slider_images as $key=>$value)
    <img src="{{ asset('home/slider_images/'.$value->image.'')}}" class="sliderImages" style="width: 150px" /> <a
        data-id="{{ $value->id }}" href="javascript:" class="delete_sliderImage">&times;</a>
    @endforeach
    @endif
  </div>
  <input type="hidden" id="uploadedImgId" name="uploadedImageIds">
</div>
    <!-- Why Choose us -->
    <div class="container-fluid text-white">
      <div class="form-group">
        <h2 class="mb-3">Why Choose Us</h2>
        <textarea class="form-control" id="why_choose_us" name="why_choose_us" rows="3">{{$why_choose_us}}</textarea>
      </div>
      <div class="form-group">
        <h2 class="mb-3">Why Choose Us(Image)</h2>
        <input type="file" class="form-control-file mb-5 mt-5" id="why_choose_us_image" name="why_choose_us_image">
        @if($why_choose_us_image != '')
        <img src="{{ asset('home/'.$why_choose_us_image.'')}}" class="sliderImages" style="width: 150px" />
        @endif
      </div>
    </div>
    <input type="submit" class="btn btn-light w-100" value="submit">
</form>
</div>

<script>

function imageDisplay() {
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {

                var data = $('#slider_images')[0].files; //this file data

            $.each(data, function(index, file){ //loop though each file
            if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
            var fRead = new FileReader(); //new filereader
            fRead.onload = (function(file){ //trigger function on successful read
            return function(e) {
                let imageId = $('#uploadedImgId').val();
                var img = '';

                    img = '<img src="'+e.target.result+'" class="sliderImages" style="width: 150px" /> <a data-id="'+ imageId +'"href="javascript:" class="delete_sliderImage">&times;</a>' //create image element
                    $('#sliderImages').append(img); //append image to output element

                };
                })(file);
                fRead.readAsDataURL(file); //URL representing the file's data.
                }

                    $('#slider_images').val('');

                });
                }else{
                    swal("Error!", "Your browser doesn't support File API!", "error");
                    //if File API is absent
                    }
                }



                $(document).on('click','.delete_sliderImage',function(){
                    let imageId = $(this).data('id');
                    deleteImage(imageId);
                    $(this).prev('img').remove();
                    $(this).remove();
                });

                function deleteImage(imageId) {
                    var part_id = $('#part_id').val();
                    $.ajax({
                            type: "POST",
                            url: "{{route('admin.delete_slider_image')}}",
                            data: {'id': imageId,'_token': '{{ csrf_token() }}', 'part_id': part_id},
                            success: function(data) {
                                if(data['success'] == true) {

                                }
                            },
                            error: function() {
                                swal("Sorry!", "There is an error", "error");
                            }
                        });
                }
    $(document).ready( function () {
      $('#slider_images').on('change', function(){

    // if($('.uploadedRecImg').length == 4) {
    //     swal("Sorry!", "You can not upload more than four images", "error");
    //     return;
    // }
    var fd = new FormData();
    var token = "{{ csrf_token() }}";
    var files = $('#slider_images')[0].files;
    console.log(files[0]);
    fd.append('slider_image',files[0]);
    fd.append('_token',token);
    
    $.ajax({
            type: "POST",
            url: "{{ route('admin.slider_images_upload') }}",
            data: fd,
            dataType: "json",
            processData: false,
            contentType: false,
            success: function(data) {
                if(data['success'] == true) {
                    id = data['id'];
                    $('input[type="hidden"][name*="uploadedImageIds"]').val(id);
                    imageDisplay();
                }
            },
            error: function() {
                swal("Sorry!", "There is an error", "error");
            }
        });



    });

    });
</script>

@endsection