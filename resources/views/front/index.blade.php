@extends('layout.app')
@section('content')

<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel" data-interval="3000">
    <div class="carousel-inner">
      @if($slider_images->count() > 0)
        @foreach ($slider_images as $key => $image)
        <div class="carousel-item @if($key == 0) active @endif">
          <img src="{{asset('home/slider_images/'.$image->image)}}" class="img-fluid" alt="Gilgit Baltistan">
        </div>
        @endforeach
      
      @else
      <div class="carousel-item active">
        <img src="{{asset('images/carasoul-1.jpg')}}" class="img-fluid" alt="Gilgit Baltistan">
      </div>
      <div class="carousel-item">
        <img src="{{asset('images/carasoul-2.jpg')}}" class="img-fluid" alt="Azad Kashmir">
      </div>
      <div class="carousel-item">
        <img src="{{asset('images/carasoul-3.jpg')}}" class="img-fluid" alt="Quetta">
      </div>
      @endif
    </div>
    <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="mt-0">
    <div class="container">
      <h1 class="text-center pt-5 text-uppercase">Why Choose Us</h1>
<div class="row">
  <div class="col-md-6">
   
    <p class="text-justify mt-3">
      @if($why_choose_us != '')
      {{$why_choose_us}}
      @else
      We are the best Tourism Planners in Pakistan. We and our
      Tourers had more than 500 trips accross Pakistan. We Cover all top Areas in Pakistan. Different Areas of Gilgit
      Baltistan We are the best Tourism Planners in Pakistan. We and our Tourers had more than 500 trips accross
      Pakistan. We Cover all top Areas in Pakistan. Different Areas of Gilgit Baltistan We are the best Tourism
      Planners in Pakistan. We and our Tourers had more than 500 trips accross Pakistan. We Cover all top Areas in
      Pakistan. Different Areas of Gilgit Baltistan We are the best Tourism Planners in Pakistan. We and our Tourers
      had more than 500 trips accross Pakistan. We Cover all top Areas in Pakistan.
      @endif
    </p>
  </div>
  <div class="col-md-6">
    @if($why_choose_us_image != '')
    <img src="{{asset('home/'.$why_choose_us_image)}}" width="300px" class="img-fluid" alt="">
      @else
    <img src="{{asset('images/carasoul-2.jpg')}}" width="300px" class="img-fluid" alt="">
    @endif
  </div>
</div>

    </div>
    <div class="container-fluid text-center p-5" style="background-color: rgb(236, 236, 236);">
      <button class="w-50 btn btn-danger p-5 mt-5 mb-5">Book A Tour</button>

    </div>
  </div>

@endsection