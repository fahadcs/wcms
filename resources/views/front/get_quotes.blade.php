@extends('layout.app')
@section('content')

<div class="container mt-5" style="max-height: 100vh !important;">
    <h1 class="text-center mb-5 text-uppercase display-4">Enter your Email for Regular Updates</h1>
    @if(session()->get('message') != '')
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{session()->get('message')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <form action="{{route('quotes_store')}}" method="POST" enctype="multipart/form-data" class="mb-5">
      @csrf
    <div class="row">
      <div class="col-md-12">
          <div class="form-group mt-4">
            <div class="text-center font-weight-bolder">
              <label for="form_email">ENTER YOUR EMAIL</label>
            </div>
              <input id="form_email" type="email" name="email" class="form-control p-4"
                  placeholder="Please enter your email to get regular update for travel and tours *" required="required"
                  data-error="Valid email is required.">
              <div class="help-block with-errors"></div>
          </div>
          <input type="submit" value="Submit" class="btn btn-success w-100 mt-3 p-4" style="margin-bottom: 120px !important;">
      </div>
    </div>
    </form>
  </div>


@endsection