@extends('layout.app')
@section('content')

<div class="container">
    <h1 class="text-center display-4 mb-5 mt-5">About Us</h1>
<div class="container">
<div class="row">
  <div class="col-md-6">
    <p>
      @if($about_us_text != '')
      {{$about_us_text}}
      @else
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
      standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
      a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
      remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
      Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions
      of Lorem Ipsum.
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
      standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
      a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
      remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
      Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions
      of Lorem Ipsum.
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
      standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
      a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
      remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
      Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions
      of Lorem Ipsum.
      @endif
    </p>
  </div>
  <div class="col-md-6">
    @if ($about_us_image != '')
      <img src="{{asset('about_us/'.$about_us_image)}}" height="550px" width="500px" alt="team">
    @else
    <img src="{{asset('images/team.jpg')}}" height="550px" width="500px" alt="team">
    @endif
  </div>
</div>
</div>
  
</div>


@endsection