@extends('layout.app')
@section('content')

<div class="container">
    <h1 class="text-center display-4 mt-5">Our Services</h1>
    <div class="row">
      <div class="col-md-4">
        <div class="card mb-3 mt-5" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>Clean Room</h2>
            <p class="card-text">@if($room != ''){{$room}} @else Some quick example text to build on the card title and make up the bulk of the card's content. @endif</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-3 mt-5" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>Night Buffet</h2>
            <p class="card-text">@if($night_buffet != ''){{$night_buffet}} @else Some quick example text to build on the card title and make up the bulk of the card's content. @endif</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-3 mt-5" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>Cricket</h2>
            <p class="card-text">@if($cricket != ''){{$cricket}} @else Some quick example text to build on the card title and make up the bulk of the card's content. @endif</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card mb-3 mt-5" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>10+ Destinations</h2>
            <p class="card-text">@if($destination != ''){{$destination}} @else Some quick example text to build on the card title and make up the bulk of the card's content. @endif</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-3 mt-5" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>3 Time Food</h2>
            <p class="card-text">@if($food != ''){{$food}} @else Some quick example text to build on the card title and make up the bulk of the card's content. @endif</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-3 mt-5" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>Hot Bath</h2>
            <p class="card-text">@if($bath != ''){{$bath}} @else Some quick example text to build on the card title and make up the bulk of the card's content. @endif</p>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection