@extends('layout.app')
@section('content')

<div class="container mt-5">
    <h1 class="text-center display-4 mb-5 text-uppercase">Our Tours</h1>
    <div class="row p-5 mb-3" style="border: 1px solid black;">
      <div class="col md-6 display-1">@if($tour_1_text != ''){{$tour_1_text}} @else Travel to Naran Valley @endif</div>
      <div class="col md-6"> @if($tour_1_image != '') <img src="{{asset('tour/'.$tour_1_image)}}" width="500px" alt=""> @else <img src="{{asset('images/team.jpg')}}" width="500px" alt=""> @endif</div>
    </div>
    <div class="row p-5 mb-3" style="border: 1px solid black;">
      <div class="col md-6"> @if($tour_2_image != '') <img src="{{asset('tour/'.$tour_2_image)}}" width="500px" alt=""> @else <img src="{{asset('images/team.jpg')}}" width="500px" alt=""> @endif </div>
      <div class="col md-6 display-1">@if($tour_2_text != ''){{$tour_2_text}} @else Travel to Naran Valley @endif</div>
    </div>
    <div class="row p-5 mb-3" style="border: 1px solid black;">
      <div class="col md-6 display-1">@if($tour_3_text != ''){{$tour_3_text}} @else Travel to Naran Valley @endif</div>
      <div class="col md-6"> @if($tour_3_image != '') <img src="{{asset('tour/'.$tour_3_image)}}" width="500px" alt=""> @else <img src="{{asset('images/team.jpg')}}" width="500px" alt=""> @endif </div>
    </div>
  </div>



@endsection