@extends('layout.app')
@section('content')

    
<div class="container">
    <h1 class="text-center mb-5 mt-5 display-4">Contact Us</h1>

    <div class="row">
      <div class="col-md-4">
        <div class="card mb-3 mt-3" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
            <h2>Mobile Contact</h2>
            <p class="card-text">@if($contact_number != ''){{$contact_number}} @else +92-308-7751002 @endif</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-3 mt-3" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>Email Address</h2>
            <p class="card-text">@if($email_address != ''){{$email_address}} @else bc1802001194@vu.edu.pk @endif</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-3 mt-3" style="border-bottom:orange 8px solid;">
          <div class="card-body pt-5 pb-5">
      <h2>Address</h2>
            <p class="card-text">@if($address != ''){{$address}} @else Chungi No.6 Vu Multan @endif</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  

    <div class="container mt-5 text-white">
      @if(session()->get('message') != '')
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session()->get('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      <form action="{{route('contact_us_store')}}" method="POST" enctype="multipart/form-data" class="mb-5">
      @csrf

          <div class="messages"></div>
      
          <div class="controls">
      
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="form_name">Firstname *</label>
                          <input id="form_name" type="text" name="fname" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
                          <div class="help-block with-errors"></div>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="form_lastname">Lastname *</label>
                          <input id="form_lastname" type="text" name="lname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
                          <div class="help-block with-errors"></div>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="form_email">Email *</label>
                          <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                          <div class="help-block with-errors"></div>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="form_message">Message *</label>
                          <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                          <div class="help-block with-errors"></div>
                      </div>
                  </div>
                  <div class="col-md-12">
                      <input type="submit" class="btn btn-success btn-send w-100 p-3 mb-5" value="Send message">
                  </div>
              </div>
          </div>
      
      </form>
    </div>


@endsection