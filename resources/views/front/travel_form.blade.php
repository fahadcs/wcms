@extends('layout.app')
@section('content')

<div class="container mt-5">
    <h1 class="text-center display-4 mb-5 text-uppercase">Travel with Us - Customize Your Destinations</h1>
    <p  class="text-center">Customize Your Travel. Our Accepted Payment Methods. BANK TRANSFER, JAZZCASH, EASYPAISA</p>
    @if(session()->get('message') != '')
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session()->get('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
   

        <div class="messages"></div>

        <div class="controls">
            <form action="{{route('book_tour')}}" method="POST" enctype="multipart/form-data" class="mb-5">
                @csrf
                <div id="render-wrap"></div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>

  
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-render.min.js"></script>
<script>
    $(document).ready( function () {
        
        var url = "{{ route('get_travel_form') }}";
         
                       $.ajax({
                           type: 'GET',
                               url: url,
                               
                               success: function (response) {
                                /* console.log(typeof(response));
                                console.log(response);
                                $(".render-wrap").formRender({response}); */
                                const code = document.getElementById("render-wrap");
                                const formData = response;
                                const addLineBreaks = html => html.replace(new RegExp("><", "g"), ">\n<");

                                // Grab markup and escape it
                                const $markup = $("<div/>");
                                $markup.formRender({ formData });

                                // set < code > innerText with escaped markup
                                $("#render-wrap").html(addLineBreaks($markup.formRender("html")));

                           }
            });
    });
  </script>


@endsection