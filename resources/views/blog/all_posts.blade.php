@extends('layout.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 mt-3">
            <form class="form-inline" action="{{route('posts_by_category')}}" >
               
                <div class="form-group mr-2">
                   
                    <select class="form-control" type="text" name="cat_id" id="cat_id" >
                       <option value="">All</option>

                        @foreach ($Categories as $cat)
                            {{-- <option value="{{$cat->id}}">{{$cat->name}}</option> --}}
                        <option class="option" @if(isset($cate_id)) @if($cate_id  == $cat->id) selected @endif  @endif value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
    <div class="row mt-3 mb-3">
        @foreach ($posts as $post)
        <div class="col-md-4 mt-3 border p-3">
            <img src="{{asset('images/post/'.$post->image)}} " width="100%">
            <h4>{{$post->title}}</h4>
            <p>{!! substr_replace($post->content, "...", 20); !!}</p>
            <a href="{{url('post/'.$post->id.'/'.$post->slug)}}">Read More</a>
        </div>
        @endforeach
        
    </div>

</div>
@endsection