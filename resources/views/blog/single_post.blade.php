@extends('layout.app')
@section('content')

<div class="container">
    <h1 class="display-4 mb-5 mt-5">{{$post->title}}</h1>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <img src="{{asset('images/post/'.$post->image)}}" >
        </div>
    </div>
<div class="row">
  <div class="col-md-12">
    <p>
     {!! $post->content !!}
    </p>
  </div>
  
</div>
<div class="row">
    <div class="col-md-12">
        <div><h4 class="mb-5 mt-3">Comment</h4></div>
        @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            @endif
        <form action="{{route('comment.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="fname">Title :</label>
                <input type="hidden" name="post_id" id="post_id" value="{{$post->id}}">
                <textarea class="form-control" id="comment" name="comment" rows="5" required></textarea>
            </div>
            <div class="form-group">
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div><h4 class="mb-5 mt-3">All Comments</h4></div>
        @foreach ($comments as $data)
            <div class="border mt-3 mb-3 p-3">
                {{$data->comment}}
            </div>
        @endforeach
    </div>
</div>
</div>
  
</div>


@endsection