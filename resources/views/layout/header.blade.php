<nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
    <a class="navbar-brand" href="#">VU MyTravler</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link home-button-border" href="{{route('index')}}">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('about_us')}}">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('our_tour')}}">Our Tours</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('travel_form')}}">Book Tour</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('our_services')}}">Our Services</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('get_quotes')}}">Get Quotes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('all_post')}}">Blogs</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('contact_us')}}">Contact Us</a>
        </li>
      </ul>
    </div>
  </nav>
<!-- /.navbar -->