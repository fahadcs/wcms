
  <!-- WCMS Vertical navbar -->
  <div class="vertical-nav bg-white" id="sidebar" style="overflow: auto">
    <div class="py-4 px-3 mb-4 bg-light">
      <div class="media d-flex align-items-center">
        <div class="media-body">
          <h4 class="m-0">Wcms Admin Panel</h4>
          <p class="font-weight-normal text-muted mb-0">bc180200194</p>
        </div>
      </div>
    </div>

    <p class="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0">Dashboard</p>

    <ul class="nav flex-column bg-white mb-0">
      <li class="nav-item">
        <a href="{{route('admin.dashboard')}}" class="nav-link text-dark bg-light">
          <i class="fa fa-th-large mr-3 text-primary fa-fw"></i>
          Home
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.about_us')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          About Us
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.contact_us')}}" class="nav-link text-dark">
          <i class="fa fa-cubes mr-3 text-primary fa-fw"></i>
          Contact Us
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.get_quotes')}}" class="nav-link text-dark">
          <i class="fa fa-bell-o mr-3 text-primary fa-fw"></i>
          Get Quotes
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.our_services')}}" class="nav-link text-dark">
          <i class="fa fa-globe mr-3 text-primary fa-fw"></i>
          Our Services
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.our_tour')}}" class="nav-link text-dark">
          <i class="fa fa-address-book-o  mr-3 text-primary fa-fw"></i>
          Tours
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.travel_form')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          Travel Form
        </a>
      </li>

      <li class="nav-item">
        <a href="{{route('admin.category.create')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          Add Category
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.category.index')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          View Categories
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.tag.create')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          Add Tag
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.tag.index')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          View Tags
        </a>
      </li>

      <li class="nav-item">
        <a href="{{route('admin.post.create')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          Add Post
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.post.index')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          View Posts
        </a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.comments.index')}}" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          Comments
        </a>
      </li>
     

    
    
      
    
    
    
      <li class="nav-item">
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="nav-link text-dark">
          <i class="fa fa-address-card mr-3 text-primary fa-fw"></i>
          Logout
        </a>
        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </div>
  <!-- End WCMS vertical navbar -->